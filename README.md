README
======

Custom components for my PhD title "High Rate Additive Manufacture using Holographic Beam Shaping - Computer-Generated Holography for Areal Additive Manufacture"

## Contributors

### Peter J. Christopher
* Email: pjc209@cam.ac.uk
* Email: peterjchristopher@gmail.com
* LinkedIn: https://www.linkedin.com/in/peterjchristopher/
* Website: www.peterjchristopher.me.uk
* Blog: http://peterjchristopher.blogspot.com/
* Address: Centre of Molecular Materials for Photonics and Electronics (CMMPE), Centre for Advanced Photonics and Electronics, University of Cambridge, 9 JJ Thomson Avenue, Cambridge, UK, CB3 0FF

